package ie.tcd.scss.helloworld.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;


@RestController
@RequestMapping
public class HelloController {

    @GetMapping("/hello")
    public String hello(){
    return "Hello, World!!";
    }
    
    @GetMapping("/howdy")
    public String howdy(){
    return "Grand! How are you?";
    }
}